<div class="mb-6 flex flex-col gap-3 sm:flex-row sm:items-center sm:justify-between">
  <h2 class="text-title-md2 font-bold text-black dark:text-white">
    {{ $pageInfo }}
  </h2>

  <nav>
    <ol class="flex items-center gap-2">
      <?php $link = ''; ?>
      @for ($i = 1; $i <= count(Request::segments()); $i++)
        @if (($i < count(Request::segments())) & ($i > 0))
          <?php $link .= '/' . Request::segment($i); ?>
          <li>
            <a class="font-medium"
              href="<?= $link ?>">{{ ucwords(str_replace('-', ' ', Request::segment($i))) . ' /' }}</a>
          </li>
        @else
          <li class="text-primary">{{ ucwords(str_replace('-', ' ', Request::segment($i))) }}</li>
        @endif
      @endfor
    </ol>
  </nav>
</div>
