<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>

  <link rel="icon" href="{{ asset('favicon.ico') }}">

  @vite('resources/css/app.css')
</head>

<body class="p-7">

  <!-- ====== Forms Section Start -->
  <div class="rounded-sm border border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark">
    <div class="flex flex-wrap items-center">
      <div class="hidden w-full xl:block xl:w-1/2">
        <div class="px-26 py-17.5 text-center">
          <a class="mb-5.5 inline-block" href="index.html">
            <img class="hidden dark:block" src="{{ asset('tailadmin/images/logo/logo.svg') }}" alt="Logo" />
            <img class="dark:hidden" src="{{ asset('tailadmin/images/logo/logo-dark.svg') }}" alt="Logo" />
          </a>

          <p class="font-medium 2xl:px-20">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit
            suspendisse.
          </p>

          <span class="mt-15 inline-block">
            <img src="{{ asset('tailadmin/images/illustration/illustration-03.svg') }}" alt="illustration" />
          </span>
        </div>
      </div>
      <div class="w-full border-stroke dark:border-strokedark xl:w-1/2 xl:border-l-2">
        <div class="w-full p-4 sm:p-12.5 xl:p-17.5">
          <span class="mb-1.5 block font-medium">Welcome Back!</span>
          <h2 class="mb-9 text-2xl font-bold text-black dark:text-white sm:text-title-xl2">
            Sign In Admin
          </h2>

          <form method="POST" action="{{ route('login') }}">
            @csrf

            <div class="mb-4">
              <label class="mb-2.5 block font-medium text-black dark:text-white">Email</label>
              <div class="relative">
                <span class="absolute left-4 top-1/2 -translate-y-1/2">
                  <svg class="fill-current" width="22" height="22" viewBox="0 0 22 22" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <g opacity="0.5">
                      <path
                        d="M19.2516 3.30005H2.75156C1.58281 3.30005 0.585938 4.26255 0.585938 5.46567V16.6032C0.585938 17.7719 1.54844 18.7688 2.75156 18.7688H19.2516C20.4203 18.7688 21.4172 17.8063 21.4172 16.6032V5.4313C21.4172 4.26255 20.4203 3.30005 19.2516 3.30005ZM19.2516 4.84692C19.2859 4.84692 19.3203 4.84692 19.3547 4.84692L11.0016 10.2094L2.64844 4.84692C2.68281 4.84692 2.71719 4.84692 2.75156 4.84692H19.2516ZM19.2516 17.1532H2.75156C2.40781 17.1532 2.13281 16.8782 2.13281 16.5344V6.35942L10.1766 11.5157C10.4172 11.6875 10.6922 11.7563 10.9672 11.7563C11.2422 11.7563 11.5172 11.6875 11.7578 11.5157L19.8016 6.35942V16.5688C19.8703 16.9125 19.5953 17.1532 19.2516 17.1532Z"
                        fill="" />
                    </g>
                  </svg>
                </span>

                <input type="email" name="email" placeholder="Enter your email" value="demo@admin.com"
                  class="w-full rounded-lg border border-stroke bg-transparent py-4 pl-11 pr-10 outline-none focus:border-primary focus-visible:shadow-none dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary" />
              </div>

              @error('email')
                <span class="mt-1 text-sm text-red-500">
                  {{ $message }}
                </span>
              @enderror
            </div>

            <div class="mb-6">
              <label class="mb-2.5 block font-medium text-black dark:text-white">Password</label>
              <div class="relative" x-data="{ isShow: false }">
                <span class="absolute left-4 top-1/2 -translate-y-1/2">
                  <svg class="fill-current" width="22" height="22" viewBox="0 0 22 22" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <g opacity="0.5">
                      <path
                        d="M16.1547 6.80626V5.91251C16.1547 3.16251 14.0922 0.825009 11.4797 0.618759C10.0359 0.481259 8.59219 0.996884 7.52656 1.95938C6.46094 2.92188 5.84219 4.29688 5.84219 5.70626V6.80626C3.84844 7.18438 2.33594 8.93751 2.33594 11.0688V17.2906C2.33594 19.5594 4.19219 21.3813 6.42656 21.3813H15.5016C17.7703 21.3813 19.6266 19.525 19.6266 17.2563V11C19.6609 8.93751 18.1484 7.21876 16.1547 6.80626ZM8.55781 3.09376C9.31406 2.40626 10.3109 2.06251 11.3422 2.16563C13.1641 2.33751 14.6078 3.98751 14.6078 5.91251V6.70313H7.38906V5.67188C7.38906 4.70938 7.80156 3.78126 8.55781 3.09376ZM18.1141 17.2906C18.1141 18.7 16.9453 19.8688 15.5359 19.8688H6.46094C5.05156 19.8688 3.91719 18.7344 3.91719 17.325V11.0688C3.91719 9.52189 5.15469 8.28438 6.70156 8.28438H15.2953C16.8422 8.28438 18.1141 9.52188 18.1141 11V17.2906Z"
                        fill="" />
                      <path
                        d="M10.9977 11.8594C10.5852 11.8594 10.207 12.2031 10.207 12.65V16.2594C10.207 16.6719 10.5508 17.05 10.9977 17.05C11.4102 17.05 11.7883 16.7063 11.7883 16.2594V12.6156C11.7883 12.2031 11.4102 11.8594 10.9977 11.8594Z"
                        fill="" />
                    </g>
                  </svg>
                </span>

                <input :type="isShow ? 'text' : 'password'" name="password" placeholder="Enter your password"
                  value="password"
                  class="w-full rounded-lg border border-stroke bg-transparent py-4 pl-11 pr-10 outline-none focus:border-primary focus-visible:shadow-none dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary" />

                <button type="button" x-on:click="isShow = !isShow" class="absolute right-4 top-1/2 -translate-y-1/2">
                  <template x-if="!isShow">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                      stroke="currentColor" class="h-6 w-6">
                      <path stroke-linecap="round" stroke-linejoin="round"
                        d="M2.036 12.322a1.012 1.012 0 0 1 0-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178Z" />
                      <path stroke-linecap="round" stroke-linejoin="round" d="M15 12a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z" />
                    </svg>
                  </template>
                  <template x-if="isShow">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                      stroke="currentColor" class="h-6 w-6">
                      <path stroke-linecap="round" stroke-linejoin="round"
                        d="M3.98 8.223A10.477 10.477 0 0 0 1.934 12C3.226 16.338 7.244 19.5 12 19.5c.993 0 1.953-.138 2.863-.395M6.228 6.228A10.451 10.451 0 0 1 12 4.5c4.756 0 8.773 3.162 10.065 7.498a10.522 10.522 0 0 1-4.293 5.774M6.228 6.228 3 3m3.228 3.228 3.65 3.65m7.894 7.894L21 21m-3.228-3.228-3.65-3.65m0 0a3 3 0 1 0-4.243-4.243m4.242 4.242L9.88 9.88" />
                    </svg>
                  </template>
                </button>
              </div>

              @error('password')
                <span class="mt-1 text-sm text-red-500">
                  {{ $message }}
                </span>
              @enderror
            </div>

            <div class="mb-4 flex items-center">
              <input id="remember" name="remember" type="checkbox"
                class="bg-gray-100 border-gray-300 dark:ring-offset-gray-800 dark:bg-gray-700 dark:border-gray-600 h-4 w-4 rounded text-blue-600 focus:ring-2 focus:ring-blue-500 dark:focus:ring-blue-600">
              <label for="remember" class="text-gray-900 dark:text-gray-300 ms-2 text-sm font-medium">Ingat Saya</label>
            </div>

            <div class="mb-5">
              <button type="submit"
                class="w-full cursor-pointer rounded-lg border border-primary bg-primary p-4 font-medium text-white transition hover:bg-opacity-90">
                Sign In
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- ====== Forms Section End -->

  <script defer src="{{ asset('tailadmin/js/bundle.js') }}"></script>
</body>

</html>
