<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\ProfileController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::prefix("admin")->middleware("auth")->group(function (): void {
    Route::get('/', [DashboardController::class, "index"])->name("admin");
    Route::get('/profile', [ProfileController::class, "index"])->name("profile");
});

